package com.itemlist.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "itemlist")
public class Item implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String itemCode;
	private String itemName;
	private double w_price;
	private double p_price;
	private long partnerId;
	
	public Item(){
		
	}
	public Item(long id, String itemCode, String itemName, double w_price,
			double p_price, long partnerId) {
		this.id = id;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.w_price = w_price;
		this.p_price = p_price;
		this.partnerId = partnerId;
	}
	
	@Id
	@Column(name = "id", nullable=false, unique=true)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "item_code")
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	@Column(name = "item_name")
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@Column(name = "w_price")
	public double getW_price() {
		return w_price;
	}
	public void setW_price(double w_price) {
		this.w_price = w_price;
	}
	@Column(name = "p_price")
	public double getP_price() {
		return p_price;
	}
	public void setP_price(double p_price) {
		this.p_price = p_price;
	}
	@Column(name = "partnerId")
	public long getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(long partnerId) {
		this.partnerId = partnerId;
	}
	

	

}
