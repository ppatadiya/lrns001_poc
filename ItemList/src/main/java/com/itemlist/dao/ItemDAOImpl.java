package com.itemlist.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.itemlist.model.Item;

@Repository
public class ItemDAOImpl implements ItemDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	/**
	 * 
	 * @return HibernateTemplate 
	 */
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	
	/**
	 * get all items
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Item> listItems() {
		List<Item> items = getHibernateTemplate().find("from Item");
		return items;
	}
}
