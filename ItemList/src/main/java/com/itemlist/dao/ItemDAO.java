package com.itemlist.dao;

import java.util.List;

import com.itemlist.model.Item;

public interface ItemDAO {

	public List<Item> listItems();
}
