package com.itemlist.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itemlist.dao.ItemDAO;
import com.itemlist.model.Item;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemDAO itemDAO;

	@Transactional
	public List<Item> listItems() {
		return itemDAO.listItems();
	}

	

}
