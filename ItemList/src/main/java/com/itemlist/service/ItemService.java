package com.itemlist.service;

import java.util.List;

import com.itemlist.model.Item;

public interface ItemService {

	public List<Item> listItems();

}
