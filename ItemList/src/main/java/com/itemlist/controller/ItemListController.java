package com.itemlist.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.itemlist.model.Item;
import com.itemlist.service.ItemService;
import com.liferay.portal.kernel.dao.search.ResultRow;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Handles requests for the ItemListPortlet view mode.
 */
@Controller
@RequestMapping("VIEW")
public class ItemListController {

	@Autowired
	private ItemService itemService;
	
	@RenderMapping
	public String defaultRender(Locale locale, Model model,RenderRequest renderRequest,RenderResponse renderResponse) throws Exception {
		processView(renderRequest, renderResponse);
		return "view";
	}
	
	private void processView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws SystemException, PortalException{
		
		ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		PortletURL portletURL = renderResponse.createRenderURL();
		
		/** create headers **/
		List<String> headers = new ArrayList<String>();
		headers.add("item-code");
		headers.add("item-name");
		headers.add("w-price");
		headers.add("p-price");
		//headers.add("partner-id");
		
		/** Create search container **/
		SearchContainer<Item> searchContainer = new SearchContainer<Item>(renderRequest, portletURL, 
				headers, "no-items-available");
		
		/** set search container data/parameters **/
		List<Item> resultList = itemService.listItems();
		searchContainer.setDelta(SearchContainer.DEFAULT_DELTA);
		int end = searchContainer.getEnd()<=resultList.size()?searchContainer.getEnd():resultList.size();
		List<Item> tempList = resultList.subList(searchContainer.getStart(), end);
		searchContainer.setTotal(resultList.size());
		searchContainer.setResults(tempList);
		
		/** Get Results row to be filled in the loop **/
		List<ResultRow> resultRows = searchContainer.getResultRows();
		int loop = 0;
		
		for(Item item: tempList){
			
			if(themeDisplay.getCompanyId() == item.getPartnerId()){
				
				/** create result row with all column to display **/
				ResultRow resultRow = new ResultRow(item,item.getId(), loop);
				
				//set item code
				resultRow.addText("center","middle",item.getItemCode());
				
				//set item name
				resultRow.addText("center","middle",item.getItemName());
				
				//set item w-price
				resultRow.addText("center","middle",String.valueOf(item.getW_price()));
				
				//set item p-price
				resultRow.addText("center","middle",String.valueOf(item.getP_price()));
				
				//set item partner id
				//resultRow.addText("center","middle",String.valueOf(item.getPartnerId()));
				
				resultRows.add(resultRow);
				loop++;
			}
		}
		
		renderRequest.setAttribute("item-list", searchContainer);
		
	}
}
