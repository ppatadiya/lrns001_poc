<%@page import="com.itemlist.model.Item"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<%
	SearchContainer<Item> searchContainer = (SearchContainer<Item>) renderRequest.getAttribute("item-list");
%>

<div style="margin-top:10px;">
	<liferay-ui:search-iterator searchContainer="<%=searchContainer %>" type="article"/>
</div>
