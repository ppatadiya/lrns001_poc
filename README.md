* This document prepared by ScalSys Team.
* The purpose of this document is to assist the use of each entity available in current repository.
* This folder contains following this to be tested.

ItemList
----------
It is a portlet with full source code.


ItemList-0.0.1-SNAPSHOT.war
---------------------------
- Ready to deploy the war file.
- Copy this war file to {LIFERAY_TOMCAT_HOME}/deploy/
- Monitor the liferay logs
- Wait till we can see "portlet is available for use", that means portlet deployed successfully.


itemslist_dump.sql
------------------
- Import this sql dump to mysql
- It has all pre loaded data to start and execute Liferay



portal-ext.properties
---------------------
- It contains neccessory properties for database connection and deployment path.